import java.util.Scanner;

class ProgramDZ31{
    
    public static int arraySum (  int [] a ) {
       	
        int arraySum = 0;
        for (int i = 0; i<a.length; i++){
            arraySum += a [i];
        }
        return arraySum;
    } 

   public static void mirror (  int [] a ){
        System.out.println ("Mirror array:");
        for (int i = 0; i < a.length / 2; i++){
             int temp = a [i];
             a[i] = a [a.length - i - 1];
             a[a.length-i-1] = temp;
        }
        for (int i = 0 ; i < a.length; i++){
            System.out.print (a[i] + " ");
        }
        System.out.println();
    }

    public static double arrayAverage ( int [] a ){
    	double arrayAverage = 0;
        for (int i = 0; i < a.length; i++){
            arrayAverage += a[i];
        }
        arrayAverage /= a.length;
        return arrayAverage;
    }

    public static void rearrangementMINMAX ( int [] a){
        int max = a[0];
        int imax = 0;
        int min = a[0];
        int imin = 0;
        for (int i = 1; i < a.length; i++) {
            if (a[i] > max) {
                max = a[i];
                imax = i;
            }
            if (a[i] < min) {
                min = a[i];
                imin = i;
            }
        }
        a[imin] = max;
        a[imax] = min;
        System.out.println ("RearrangementMINMAX array:");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        } 
        System.out.println ();
    }

    public static void bubble ( int [] a ){
        int x , b;
		for ( x = 1; x < a.length; x++){
			for (b = a.length - 1; b >= x; b--){
				int t;
				if (a[b-1] > a[b]){
					t = a[b - 1];
					a [b-1] = a[b];
					a[b] = t;
				}

			}
		}
		System.out.println ("Bubblesort array:");
		for (int i = 0; i < a.length; i++){
			System.out.print (a[i]+ " ");
		}
		System.out.println ();

    }

    public static int number ( int [] a ){
        int number = 0;
        for (int d : a) {
            number = 10 * number + d;
        }
        return number;
    }
 
 	public static void main (String[] args) {
		Scanner scanner = new Scanner( System.in);
		System.out.println ("Array size: ");
    	int n = scanner.nextInt();
    	System.out.println ("Array elements: ");
        int [] a = new int [n];

        for (int i = 0; i < a.length; i++){
            a[i] = scanner.nextInt();
        }

        System.out.println ("New array:");
        for (int i = 0 ; i < a.length; i++){
			
			System.out.print (a[i] + " ");
		}
		System.out.println ();
      
        int result = arraySum (a);
        System.out.println ("ArraySum" + " =" + " " + result);
        
        mirror (a);
        
        double result1 = arrayAverage (a);
        System.out.println ("ArrayAverage" + " =" + " " + result1);
        
        rearrangementMINMAX (a);
        
        bubble (a);
        
        int result2 = number(a);
        System.out.println ("Number" + " =" + " " + result2);
    }
  
	
}