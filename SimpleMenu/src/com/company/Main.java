package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        System.out.println ("Menu");
        System.out.println("0. Say Hello");
        System.out.println("2. Say Bye");
        System.out.println("3. Say Bye");
        System.out.println("4. Exit");

        Scanner scanner = new Scanner(System.in);

        int command = scanner.nextInt();

        switch (command) {
            case 1: {
                System.out.println("Hello!");
                break;
            }
            case 2: {
                System.out.println("Bye");
                break;
            }
            case 3: {
                System.out.println("Marsel");
                break;
            }
            case 4:{
                System.exit(0);
            }
        }
    }
}
