import java.util.Scanner;

class ProgramDZ3{
	public static void main(String[] args) {
        System.out.println("Enter numbers: ");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int mult = 1;

        while ( n != 0){
            int sum = 0;
            int copyNumber = n;
            while (copyNumber != 0){
                sum += copyNumber % 10;
                copyNumber /= 10;
            }
            boolean isPrime = true;
            for (int factor = 2; factor <sum; factor++){
                if (sum % factor == 0){
                    isPrime = false;
                    break;
                }
            }
            if (isPrime){
                mult *= n;
            }
            n = scanner.nextInt();

        }
        
        System.out.println (mult);
    }

}