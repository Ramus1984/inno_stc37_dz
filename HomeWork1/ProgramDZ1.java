class ProgramDZ1{
	public static void main(String[] args) {
		int number = Integer.parseInt(args[0]);
		int digitsSum = 0;
		final int BASE = 10;
			
		digitsSum += number % BASE;
		number /= BASE; 
		
		digitsSum += number % BASE;
		number /= BASE; 

        digitsSum += number % BASE;
		number /= BASE; 

		digitsSum += number % BASE;
		number /= BASE; 

		digitsSum += number % BASE;
		number /= BASE; 

		System.out.println(digitsSum);
	
	}
}