import java.util.Scanner;

class ProgramDZ41{
    public static boolean isPowerOfTwo( int n) {
        if ((n > 0) && ((n & (n - 1)) == 0)) {
            return true;
        }
        else {
            return false;
        }
    }
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        System.out.println ("Enter num: ");
        int n = in.nextInt();
        boolean m = isPowerOfTwo (n);
        System.out.println (m);
	}
}