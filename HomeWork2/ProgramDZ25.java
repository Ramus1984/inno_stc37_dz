import java.util.Arrays;
import java.util.Scanner;

class ProgramDZ25 {// метод пузырька
	public static void main(String[] args) {
		Scanner scanner = new Scanner (System.in);
		System.out.println ("Array size: ");
		int n = scanner.nextInt ();
		int array [] = new int [n];
		
	
		for (int i = 0; i < array.length; i++){
			array[i] = scanner.nextInt ();
		}
		System.out.println ();
		
		System.out.print ("Source array: ");
		for (int i = 0; i < array.length; i++){
			System.out.print (array[i] + " ");
		}
		System.out.println ();
		
		int a , b;
		for ( a = 1; a < array.length; a++){
			for (b = array.length - 1; b >= a; b--){
				int t;
				if (array[b-1] > array[b]){
					t = array[b - 1];
					array [b-1] = array[b];
					array[b] = t;
				}

			}
		}
		System.out.print ("Sorted array: ");
		for (int i = 0; i < array.length; i++){
			System.out.print (array[i]+ " ");
		}
		System.out.println ();
	}
}