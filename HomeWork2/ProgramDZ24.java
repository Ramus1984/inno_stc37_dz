import java.util.Scanner;
import java.util.Arrays;

class ProgramDZ24{// перемена min max массива
	public static void main(String[] args) {
		Scanner scanner = new Scanner( System.in);
         int n = scanner.nextInt();
         int array [] = new int [n];

         int min = array[0];
         int imax = 0;
         int max = array[0];
         int imin = 0;
         
        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        } 

        System.out.println ("New array:");
        for (int i = 0 ; i < array.length; i++){
			
			System.out.print (array[i] + " ");
		}
		System.out.println ();

        for (int i = 1; i < array.length; i++) {
            if (array[i] > max) {
                max = array[i];
                imax = i;
            }
            if (array[i] < min) {
                min = array[i];
                imin = i;
            }
        }
        array[imin] = max;
        array[imax] = min;
        System.out.println ("RearrangementMINMAX array:");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        } 
        System.out.println ();
    }

}