import java.util.Scanner;

class ProgramDZ23{//вывод среднеарифметического элементов массива
	public static void main(String[] args) {
		Scanner scanner = new Scanner( System.in);
        int n = scanner.nextInt();
        int array [] = new int [n];
        
        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        
        double arrayAverage = 0;
        for (int i = 0; i < array.length; i++){
            arrayAverage += array[i];
            
        }
        arrayAverage /= array.length;
        System.out.println(arrayAverage);
	}
}
