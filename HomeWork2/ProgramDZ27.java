import java.util.Scanner;

class ProgramDZ27{// заполнение массива спиралью по часовой стрелке как в ДЗ
	public static void main(String[] args) {
		
        int m = Integer.parseInt(args[0]);
        int n = Integer.parseInt(args[1]);

        int s = Integer.parseInt(args[2]);
        
        int[][] array = new int[m][n];

        for (int i = 0; i < n; i++) {
            array[0][i] = s;
            s++;
        }
        for (int j = 1; j < m; j++) {
            array[j][n - 1] = s;
            s++;
        }
        for (int i = n - 2; i >= 0; i--) {
            array[m - 1][i] = s;
            s++;
        }
        for (int j = m - 2; j > 0; j--) {
            array[j][0] = s;
            s++;
        }

        int c = 1;
        int d = 1;

        while (s < m * n) {
            while (array[c][d + 1] == 0) {
                array[c][d] = s;
                s++;
                d++;
            }
            while (array[c + 1][d] == 0) {
                array[c][d] = s;
                s++;
                c++;
            }
            while (array[c][d - 1] == 0) {
                array[c][d] = s;
                s++;
                d--;
            }
            while (array[c - 1][d] == 0) {
                array[c][d] = s;
                s++;
                c--;
            }
        }
        for (int j = 0; j < m; j++) {
            for (int y = 0; y < n; y++) {
                if (array[j][y] == 0) {
                    array[j][y] = s;
                }
            }
        }
        for (int j = 0; j < m; j++) {
            for (int i = 0; i < n; i++) {
            	System.out.printf("%d\t",array[j][i]);
            }
            System.out.println( );
		}
	}
}