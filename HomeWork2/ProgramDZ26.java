class ProgramDZ26{// преобразования массива в число - метод Горнера. хочу разобрать его на консультации
	public static void main(String[] args) {
		int [] array = {4, 2, 3, 5, 7};
        
        int number = 0;
        for (int d : array) {
            number = 10 * number + d;
        }
        System.out.println(number);
	}
}